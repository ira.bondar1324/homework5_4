import React from "react";
import ProductList from "../../Components/ProductList/ProductList";
import Modal from "../../Components/Modal/Modal";
import PropTypes from "prop-types";

const CartPages=()=>{

    return(
        <>
            <ProductList type='cart'/>
            <Modal/>
        </>
    );
}

export default CartPages;