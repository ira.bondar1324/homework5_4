import { TOGGLE_MODAL,TYPE_MODAL} from "./actions";

export const toggleModal = (value)=>({type:TOGGLE_MODAL, payload:value});

export const typeModal = (value)=>({type:TYPE_MODAL, payload:value});