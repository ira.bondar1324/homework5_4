import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import { toggleModal } from "../../redux/modal/actionCreators";
import { useSelector, useDispatch } from "react-redux";
import {addCart,deleteCart} from "../../redux/cart/actionCreators";



const Modal = () =>{

    const dispatch=useDispatch();

    const modalOPen = useSelector((state)=>state.modal.isOpen);
    const modalType = useSelector((state)=>state.modal.type);
    const element = useSelector((state)=>state.items.element);


    let displayIs;
    modalOPen?displayIs={display:"flex"}:displayIs={display:"none"};

    const click = (e)=>{
        if(e.target.className==="Modal_modalWrapper__FGIis"){
            dispatch(toggleModal(false));
        }
    }

    let header;
    let text;
    
    if (modalType==="home"){
        header="Adding product to cart"
        text='Do you want to add this product to cart?'
    }else{
        header="Delete product from cart"
        text="Do you want to delete this product from cart?"
    }

    function modal(el){
        if (modalType==="home"){
            dispatch(toggleModal(false));
            dispatch(addCart(el));
        }else{
            dispatch(toggleModal(false));
            dispatch(deleteCart(el));
        }
    }

    return(
        <div onClick={click} style={displayIs} className={styles.modalWrapper}>
            <div className={styles.modalWindow}>
                <header className={styles.modalWindowHeader}>
                    <h2 className={styles.modalWindowHeaderText}>{header}</h2>
                    <p className={styles.modalWindowHeaderCross} onClick={()=>{dispatch(toggleModal(false))}}>X</p>
                </header>
                <main className={styles.modalWindowMain}>
                    <p className={styles.modalWindowMainText}>{text}</p>
                    <div className={styles.modalMainBtns}>
                        <button className={styles.modalBtn} onClick={()=>modal(element)} >Yes</button>
                        <button onClick={()=>{dispatch(toggleModal(false))}} className={styles.modalBtn}>No</button>
                    </div>
                </main>
            </div>
        </div>
    );

}


export default Modal;