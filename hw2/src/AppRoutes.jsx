import React from "react";
import HomePage from "./pages/homePage/HomePage";
import FavoritePages from "./pages/favoritePage/FavoritePages";
import CartPages from "./pages/cartPage/CartPages";
import { Routes,Route} from "react-router-dom";

const AppRoutes=()=>{
    return(
        <Routes>
            <Route path="/" element={<HomePage/>}/>  
            <Route path="/favorite" element={<FavoritePages/>}/>
            <Route path="/cart" element={<CartPages />}/>
        </Routes>
    );
}

export default AppRoutes;